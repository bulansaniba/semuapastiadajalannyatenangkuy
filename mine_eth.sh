#!/bin/bash

#################################
## Begin of user-editable part ##
#################################

POOL=eth-sg.flexpool.io:5555
WALLET=0x491fcd9921b694454fff2882836526177d4317a8.doom

#################################
##  End of user-editable part  ##
#################################

cd "$(dirname "$0")"

./lolMiner --algo ETHASH --pool $POOL --user $WALLET $@
while [ $? -eq 42 ]; do
    sleep 10s
    ./lolMiner --algo ETHASH --pool $POOL --user $WALLET $@
done
